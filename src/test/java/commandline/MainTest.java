package commandline;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MainTest {

    @Test
    public void testMain() {
        String[] strings = {"test 1", "test 2", "test 3"};

        assertNotNull(strings);
        assertEquals(strings.length, 3);
    }
}